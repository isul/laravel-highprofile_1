<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/',function () {
  if (Auth::user()){
    return Redirect::to('/profile');
  }else {
    return view('auth.login');
  }
});


Auth::routes();

Route::get('/profile', 'MembersController@profile');

// --------------------- Admin Route -------------------------------

Route::get('/admin', 'AdminsController@index');

// ---------------------End of Admin Route -------------------------


// --------------------- Role Route -------------------------------

// ---------------------End of Role Route -------------------------


// --------------------- Member Route -------------------------------
Route::get('/newmember','MembersController@create');

Route::get('/members/edit/{id}',[
  'as' => 'members.edit',
  'uses' => 'MembersController@edit'
]);

Route::post('/members/store',[
  'as' => 'members.store',
  'uses' => 'MembersController@store'
]);

Route::put('/members/update/{id}',[
  'as' => 'members.update',
  'uses' => 'MembersController@update'
]);


Route::delete('/members/destroy/{id}',[
  'as' => 'members.destroy',
  'uses' => 'MembersController@destroy'
]);
// ---------------------End of Member Route -------------------------


// --------------------- Skill Route -------------------------------
Route::get('/members/skills','SkillsController@index');
Route::get('/members/skills/new','SkillsController@create');

Route::get('/members/skills/edit/{id}',[
  'as' => 'skills.edit',
  'uses' => 'SkillsController@edit'
]);

Route::post('/members/skills/store',[
  'as' => 'skills.store',
  'uses' => 'SkillsController@store'
]);


Route::put('/members/skills/update/{id}',[
  'as' => 'skills.update',
  'uses' => 'SkillsController@update'
]);

Route::delete('/members/skills/destroy/{id}',[
  'as' => 'skills.destroy',
  'uses' => 'SkillsController@destroy'
]);
// ---------------------End of Skill Route -------------------------


// --------------------- Education Route -------------------------------
Route::get('/members/educations','EducationsController@index');
Route::get('/members/educations/new','EducationsController@create');

Route::get('/members/educations/edit/{id}',[
  'as' => 'educations.edit',
  'uses' => 'EducationsController@edit'
]);

Route::post('/members/educations/store',[
  'as' => 'educations.store',
  'uses' => 'EducationsController@store'
]);


Route::put('/members/educations/update/{id}',[
  'as' => 'educations.update',
  'uses' => 'EducationsController@update'
]);

Route::delete('/members/educations/destroy/{id}',[
  'as' => 'educations.destroy',
  'uses' => 'EducationsController@destroy'
]);
// ---------------------End of Education Route -------------------------


// --------------------- Achievement Route -------------------------------
Route::get('/members/achievements','AchievementsController@index');
Route::get('/members/achievements/new','AchievementsController@create');

Route::get('/members/achievements/edit/{id}',[
  'as' => 'achievements.edit',
  'uses' => 'AchievementsController@edit'
]);

Route::post('/members/achievements/store',[
  'as' => 'achievements.store',
  'uses' => 'AchievementsController@store'
]);


Route::put('/members/achievements/update/{id}',[
  'as' => 'achievements.update',
  'uses' => 'AchievementsController@update'
]);

Route::delete('/members/achievements/destroy/{id}',[
  'as' => 'achievements.destroy',
  'uses' => 'AchievementsController@destroy'
]);
// ---------------------End of Achievement Route -------------------------


// --------------------- Experience Route -------------------------------
Route::get('/members/experiences','ExperiencesController@index');
Route::get('/members/experiences/new','ExperiencesController@create');

Route::get('/members/experiences/edit/{id}',[
  'as' => 'experiences.edit',
  'uses' => 'ExperiencesController@edit'
]);

Route::post('/members/experiences/store',[
  'as' => 'experiences.store',
  'uses' => 'ExperiencesController@store'
]);


Route::put('/members/experiences/update/{id}',[
  'as' => 'experiences.update',
  'uses' => 'ExperiencesController@update'
]);

Route::delete('/members/experiences/destroy/{id}',[
  'as' => 'experiences.destroy',
  'uses' => 'ExperiencesController@destroy'
]);
// ---------------------End of Experience Route -------------------------
