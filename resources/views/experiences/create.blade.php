@extends('layouts.app')

@section('content')
  @if(Session::has('flash_message'))
    <div class="alert alert-success alert-dismissable">
      <button type="button" class="close" data-dismiss="alert">&times;</button>
      {{Session::get('flash_message')}}
    </div>
  @endif
  <h1> Add your Experience <a class="btn-sm btn-primary pull-right" href="/profile"> Back to profile</a></h1>

{!! Form::open(['method' => 'POST','route' => 'experiences.store', 'class' => 'form-horizontal']) !!}

        <div class="col-md-8">
          <div class="form-group{{ $errors->has('company_name') ? ' has-error' : '' }}">
              {!! Form::label('company_name', 'Company Name') !!}
              {!! Form::text('company_name', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('company_name') }}</small>
          </div>

          <div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
              {!! Form::label('role', 'Job Title') !!}
              {!! Form::text('role', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('role') }}</small>
          </div>


          <div class="form-group{{ $errors->has('date_from') ? ' has-error' : '' }}">
              {!! Form::label('date_from', 'Date Started') !!}
              {!! Form::date('date_from', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('date_from') }}</small>
          </div>

          <div class="form-group{{ $errors->has('date_to') ? ' has-error' : '' }}">
              {!! Form::label('date_to', 'Date to') !!}
              {!! Form::date('date_to', null, ['class' => 'form-control', 'id'=>'date_to']) !!}
              <small class="text-danger">{{ $errors->first('date_to') }}</small>
          </div>

          <div class="form-group{{ $errors->has('job_desc') ? ' has-error' : '' }}">
              {!! Form::label('job_desc', 'Job Description') !!}
              {!! Form::textarea('job_desc', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('job_desc') }}</small>
          </div>

          <br>

          <div class="pull-right">
            {!! Form::reset("Reset", ['class' => 'btn btn-default']) !!}
            {!! Form::submit("Add Experience", ['class' => 'btn btn-primary']) !!}
          </div>

        </div>



{!! Form::close() !!}
@endsection
