@extends('layouts.app')

@section('content')
  @if(Session::has('flash_message'))
    <div class="alert alert-success alert-dismissable">
      <button type="button" class="close" data-dismiss="alert">&times;</button>
      {{Session::get('flash_message')}}
    </div>
  @endif
  <h1> Add your Achievement <a class="btn-sm btn-primary pull-right" href="/profile"> Back to profile</a></h1>

{!! Form::open(['method' => 'POST','route' => 'achievements.store', 'class' => 'form-horizontal']) !!}

        <div class="col-md-8">
          <div class="form-group{{ $errors->has('event') ? ' has-error' : '' }}">
              {!! Form::label('event', 'Event Name') !!}
              {!! Form::text('event', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('event') }}</small>
          </div>

          <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
              {!! Form::label('description', 'Description') !!}
              {!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => '4']) !!}
              <small class="text-danger">{{ $errors->first('description') }}</small>
          </div>

          <div class="form-group{{ $errors->has('given_by') ? ' has-error' : '' }}">
              {!! Form::label('given_by', 'Awarded By') !!}
              {!! Form::text('given_by', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('given_by') }}</small>
          </div>

          <div class="form-group{{ $errors->has('date_acquired') ? ' has-error' : '' }}">
              {!! Form::label('date_acquired', 'Date Acquired') !!}
              {!! Form::date('date_acquired', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('date_acquired') }}</small>
          </div>

          <br>

          <div class="pull-right">
            {!! Form::reset("Reset", ['class' => 'btn btn-default']) !!}
            {!! Form::submit("Add Achievement", ['class' => 'btn btn-primary']) !!}
          </div>

        </div>



{!! Form::close() !!}
@endsection
