@extends('layouts.app')

@section('content')
  @if(Session::has('flash_message'))
    <div class="alert alert-success alert-dismissible alert">
      <button type="button" class="close" data-dismiss="alert">&times;</button>
      {{Session::get('flash_message')}}
    </div>
  @endif
  <h1>Edit Achievement  <a href="/profile" class="btn-sm btn-primary pull-right">< Back to profile</a></h1>

  <div class="col-md-8">
    {!! Form::model($experience, ['route' => ['achievements.update', $achievement['id']], 'method' => 'PUT', 'class' => 'form-horizontal'] ) !!}

        <div class="col-md-8">

          <<div class="form-group{{ $errors->has('event') ? ' has-error' : '' }}">
              {!! Form::label('event', 'Event Name') !!}
              {!! Form::text('event', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('event') }}</small>
          </div>

          <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
              {!! Form::label('description', 'Description') !!}
              {!! Form::text('description', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('description') }}</small>
          </div>

          <div class="form-group{{ $errors->has('given_by') ? ' has-error' : '' }}">
              {!! Form::label('given_by', 'Awarded By') !!}
              {!! Form::text('given_by', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('given_by') }}</small>
          </div>

          <div class="form-group{{ $errors->has('date_acquired') ? ' has-error' : '' }}">
              {!! Form::label('date_acquired', 'Date Acquired') !!}
              {!! Form::date('date_acquired', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('date_acquired') }}</small>
          </div>
          
          <br>

          <div class="pull-right">

            {!! Form::submit("Save", ['class' => 'btn btn-primary']) !!}
          </div>

        </div>
    {!! Form::close() !!}

  </div>

@endsection
