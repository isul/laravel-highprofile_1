@extends('layouts.app')

@section('content')

@if(Session::has('flash_message'))
  <div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    {{Session::get('flash_message')}}
  </div>
@endif
<div class="row">
  <div class="panel panel-default" id="profile">
    <div class="panel-heading">
      <h3 class="panel-title">Personal Information</h3>
    </div>
    <div class="panel-body">
      @if ($member)
          <div class="col-sm-3">
            @if($member['image']!=null)
                  <img height="250"  width="250"  class="img-circle" src="data:image;base64,{{ base64_encode($member['image']) }}" alt="" />
            @else
                <img height="250" width="250" src="{{url('/images/profile.png')}}">
            @endif
          </div>
          <div class="col-sm-9 pull-bottom">
            <br><br>
            <br><br>
            <h3><strong></strong> {{ $member['fname'] . ' ' . $member['mname'] . ' ' . $member['lname'] }}</h3>
            <h5><strong>Email: </strong> {{ Auth::user()->email }}</h5>
            <h5><strong>Phone No.: </strong> {{ $member['phone'] }}</h5>
            <h5><strong>Address: </strong> {{ $member['address'] }}</h5><br>

            <a href="{{route('members.edit',$member['id'])}}">Edit Profile</a>
          </div>
      @else

        <div class="col-sm-3">
          <img height="250" width="250" src="{{url('/images/profile.png')}}">
        </div>
        <div class="col-sm-9">
          <h4>You're Personal information is not yet set. Fill-out Personal information <a href="{{ url('/newmember')}}">Here</a></h4>
        </div>
      @endif
    </div>
  </div>
</div>

@if ($member)
<div class="row">
  <div class="panel panel-default" id="experience">
    <div class="panel-heading">
      <h3 class="panel-title">Experience</h3>
    </div>
    <div class="panel-body">
      @if ($experiences!='[]')
        @foreach ($experiences as $experience)
          <div class="panel panel-default">
            <div class="panel-body">
              <h4><strong>{{ $experience->company_name }}</strong><div class="pull-right text-muted">
                <em><small>{{ date('M-d-Y', strtotime($experience->date_from)) . ' - ' . date('M-d-Y', strtotime($experience->date_to)) }}</small></em>
              </div></h4>
              <h5>{{ $experience->role }}</h5>
              <br>
              <p class="text-muted">
                <strong>Job Description:</strong> {{ $experience->job_desc }}
              </p>

              {!! Form::open(['method' => 'DELETE', 'route' => ['experiences.destroy', $experience->id]]) !!}
                      <small><a href="{{ route('experiences.edit', $experience->id) }}" class="btn btn-success btn-xs">Edit Experience</a></small>
                      {!! Form::submit("Remove Experience", ['class' => 'btn btn-danger btn-xs']) !!}
              {!! Form::close() !!}
            </div>
          </div>
        @endforeach

        <a href="{{ url('/members/experiences/new')}}" class="btn btn-primary btn-sm">Add Experience</a>
      @else
        <h4>You're don't have any experience information on our record. Set it up <a href="{{ url('/members/experiences/new')}}">here!</a></h4>
      @endif
    </div>
  </div>
</div>
<div class="row">
  <div class="panel panel-default" id="achievement">
    <div class="panel-heading">
      <h3 class="panel-title">Achievements</h3>
    </div>
    <div class="panel-body">
      @if ($achievements!='[]')
        @foreach ($achievements as $achievement)
          <div class="panel panel-default">
            <div class="panel-body">
              <h4><strong>{{ $achievement->description }}</strong><div class="pull-right text-muted">
                <em><small>Date Acquired: {{ date('M-d-Y', strtotime($achievement->date_acquired)) }}</small></em>
              </div></h4>
              <h5><strong>Event:</strong> {{ $achievement->event }}</h5>

              <p class="">
                <strong>Awarde By:</strong> {{ $achievement->description }}
              </p>

              {!! Form::open(['method' => 'DELETE', 'route' => ['achievements.destroy', $achievement->id]]) !!}
                      <small><a href="{{ route('achievements.edit', $achievement->id) }}" class="btn btn-success btn-xs">Edit Achievement</a></small>
                      {!! Form::submit("Remove Achievement", ['class' => 'btn btn-danger btn-xs']) !!}
              {!! Form::close() !!}
            </div>
          </div>
        @endforeach

        <a href="{{ url('/members/achievements/new')}}" class="btn btn-primary btn-sm">Add Achievement</a>

      @else
        <h4>You're don't have any achievement information on our record. Set it up <a href="{{ url('/members/achievements/new')}}">here!</a></h4>
      @endif
    </div>
  </div>
</div>
<div class="row">
  <div class="panel panel-default" id="skill">
    <div class="panel-heading">
      <h3 class="panel-title">Skills</h3>
    </div>
    <div class="panel-body">
      @if ($skills!='[]')
          <table class="table">
            <thead>
              <tr>
                <th>Skill Name</th>
                <th>Level</th>
                <th>Action</th>
              </tr>
            </thead>
            @foreach ($skills as $skill)
              <tr>
                <td>
                  {{ $skill->skill_name }}
                </td>
                <td style="width: 70%;">
                  <div class="progress">
                    <div class="progress-bar" style="width: {{ $skill->level }}%;">{{ $skill->level }}%</div>
                  </div>
                  <h6><strong>Skill Description: </strong>{{ $skill->description }}</h6>
                </td>
                <td>
                  {!! Form::open(['method' => 'DELETE', 'route' => ['skills.destroy', $skill->id]]) !!}
                          <small><a href="{{ route('skills.edit', $skill->id) }}" class="btn btn-success btn-xs">Edit Skill</a></small>
                          {!! Form::submit("Remove Achievement", ['class' => 'btn btn-danger btn-xs']) !!}
                  {!! Form::close() !!}
                </td>
              </tr>
            @endforeach
          </table>

          <a href="{{ url('/members/skills/new')}}" class="btn btn-primary btn-sm">Add Skill</a>
      @else
          <h4>You're don't have any skill information on our record. Set it up <a href="{{ url('/members/skills/new')}}">here!</a></h4>
      @endif
    </div>
  </div>
</div>
<div class="row">
  <div class="panel panel-default" id="education">
    <div class="panel-heading">
      <h3 class="panel-title">Education</h3>
    </div>
    <div class="panel-body">
      @if ($educations!='[]')
        @foreach ($educations as $education)
          <div class="panel panel-default">
            <div class="panel-body">
              <h4><strong>{{ $education->educ_level }} : </strong> <em>{{ $education->school }}</em><div class="pull-right text-muted">
                <em><small>{{ date('M-d-Y', strtotime($education->date_from)) . ' - ' . date('M-d-Y', strtotime($education->date_to)) }}</small></em>
              </div></h4>
              <h5 class="text-muted"><strong></strong> {{ $education->course }}</h5>

              {!! Form::open(['method' => 'DELETE', 'route' => ['educations.destroy', $education->id]]) !!}
                      <small><a href="{{ route('educations.edit', $education->id) }}" class="btn btn-success btn-xs">Edit Education</a></small>
                      {!! Form::submit("Remove Education", ['class' => 'btn btn-danger btn-xs']) !!}
              {!! Form::close() !!}
            </div>
          </div>
        @endforeach

        <a href="{{ url('/members/educations/new')}}" class="btn btn-primary btn-sm">Add Education</a>
      @else
          <h4>You're don't have any education information on our record. Set it up <a href="{{ url('/members/educations/new')}}">here!</a></h4>
      @endif
    </div>
  </div>
</div>
@endif
@endsection
