@extends('layouts.app')

@section('content')
  @if(Session::has('flash_message'))
    <div class="alert alert-success alert-dismissible alert">
      <button type="button" class="close" data-dismiss="alert">&times;</button>
      {{Session::get('flash_message')}}
    </div>
  @endif
  <h1>Edit Personal Information  <a href="/profile" class="btn-sm btn-primary pull-right">< Back to profile</a></h1>

  <div class="col-md-8">
    {!! Form::model($member, ['route' => ['members.update', $member['id']], 'method' => 'PUT' ,'files' => true, 'class' => 'form-horizontal'] ) !!}
        @if($member['image']!=null)
            <img class="img-circle" height="250" width="250" src="data:image;base64,{{ base64_encode($member->image) }}" alt="" />
        @else
            <img class="img-circle"  height="250" width="250" src="{{ url('/images/profile.png') }}" id="uploadedimage">
        @endif

        <div class="col-md-8">

          <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">

              {!! Form::label('image', 'Displayed Photo') !!}
              {!! Form::file('image')
              !!}
              <p class="help-block">Image Here</p>
              <small class="text-danger">{{ $errors->first('image') }}</small>
          </div>

          <div class="form-group{{ $errors->has('lname') ? ' has-error' : '' }}">
              {!! Form::label('lname', 'Last Name') !!}
              {!! Form::text('lname', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('lname') }}</small>
          </div>

          <div class="form-group{{ $errors->has('fname') ? ' has-error' : '' }}">
              {!! Form::label('fname', 'First Name') !!}
              {!! Form::text('fname', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('fname') }}</small>
          </div>

          <div class="form-group{{ $errors->has('mname') ? ' has-error' : '' }}">
              {!! Form::label('mname', 'Middle Name') !!}
              {!! Form::text('mname', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('mname') }}</small>
          </div>

          <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
              {!! Form::label('gender', 'Input label') !!}
              {!! Form::select('gender', ['Male'=>'Male', 'Female'=>'Female'], null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('gender') }}</small>
          </div>

          <div class="form-group{{ $errors->has('birthdate') ? ' has-error' : '' }}">
              {!! Form::label('birthdate', 'Date') !!}
              {!! Form::date('birthdate', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('birthdate') }}</small>
          </div>

          <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
              {!! Form::label('description', 'Address') !!}
              {!! Form::text('address', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('address') }}</small>
          </div>

          <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
              {!! Form::label('phone', 'Phone Number') !!}
              {!! Form::text('phone', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('phone') }}</small>
          </div>

          <br>

          <div class="pull-right">

            {!! Form::submit("Save", ['class' => 'btn btn-primary']) !!}
          </div>

        </div>
    {!! Form::close() !!}

  </div>

@endsection
