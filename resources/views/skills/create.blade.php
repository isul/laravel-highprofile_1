@extends('layouts.app')

@section('content')
  @if(Session::has('flash_message'))
    <div class="alert alert-success alert-dismissable">
      <button type="button" class="close" data-dismiss="alert">&times;</button>
      {{Session::get('flash_message')}}
    </div>
  @endif
  <h1> Add your Skill <a class="btn-sm btn-primary pull-right" href="/profile"> Back to profile</a></h1>

{!! Form::open(['method' => 'POST','route' => 'skills.store', 'class' => 'form-horizontal']) !!}

        <div class="col-md-8">
          <div class="form-group{{ $errors->has('skill_name') ? ' has-error' : '' }}">
              {!! Form::label('skill_name', 'Skill Name') !!}
              {!! Form::text('skill_name', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('skill_name') }}</small>
          </div>

          <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
              {!! Form::label('description', 'Skill Description') !!}
              {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('description') }}</small>
          </div>

          <div class="form-group{{ $errors->has('level') ? ' has-error' : '' }}">
              {!! Form::label('level', 'Level (%)') !!}
              {!! Form::number('level', null, ['class' => 'form-control', 'max' => '100', 'min' => '0', 'step'=>'5']) !!}
              <small class="text-danger">{{ $errors->first('level') }}</small>
          </div>

          <br>

          <div class="pull-right">
            {!! Form::reset("Reset", ['class' => 'btn btn-default']) !!}
            {!! Form::submit("Add Skill", ['class' => 'btn btn-primary']) !!}
          </div>

        </div>



{!! Form::close() !!}
@endsection
