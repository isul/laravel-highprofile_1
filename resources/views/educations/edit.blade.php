@extends('layouts.app')

@section('content')
  @if(Session::has('flash_message'))
    <div class="alert alert-success alert-dismissible alert">
      <button type="button" class="close" data-dismiss="alert">&times;</button>
      {{Session::get('flash_message')}}
    </div>
  @endif
  <h1>Edit Education  <a href="/profile" class="btn-sm btn-primary pull-right">< Back to profile</a></h1>

  <div class="col-md-8">
    {!! Form::model($education, ['route' => ['educations.update', $education['id']], 'method' => 'PUT', 'class' => 'form-horizontal'] ) !!}

        <div class="col-md-8">

          <div class="form-group{{ $errors->has('educ_level') ? ' has-error' : '' }}">
              {!! Form::label('educ_level', 'Level of Education') !!}
              {!! Form::select('educ_level', [
                'Post Graduation'=>'Post Graduation',
                'College'=>'College',
                'Certification'=>'Certification',
                'Vocational'=>'Vocational',
                'High School'=>'High School',
                'Elementary'=>'Elementary'
              ], null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('educ_level') }}</small>
          </div>
          <div class="form-group{{ $errors->has('course') ? ' has-error' : '' }}">
              {!! Form::label('course', 'Course') !!}
              {!! Form::text('course', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('course') }}</small>
          </div>

          <div class="form-group{{ $errors->has('school') ? ' has-error' : '' }}">
              {!! Form::label('school', 'School') !!}
              {!! Form::text('school', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('school') }}</small>
          </div>


          <div class="form-group{{ $errors->has('date_from') ? ' has-error' : '' }}">
              {!! Form::label('date_from', 'Date Started') !!}
              {!! Form::date('date_from', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('date_from') }}</small>
          </div>

          <div class="form-group{{ $errors->has('date_to') ? ' has-error' : '' }}">
              {!! Form::label('date_to', 'Date to') !!}
              {!! Form::date('date_to', null, ['class' => 'form-control', 'id'=>'date_to']) !!}
              <small class="text-danger">{{ $errors->first('date_to') }}</small>
          </div>


          <br>

          <div class="pull-right">

            {!! Form::submit("Save", ['class' => 'btn btn-primary']) !!}
          </div>

        </div>
    {!! Form::close() !!}

  </div>

@endsection
