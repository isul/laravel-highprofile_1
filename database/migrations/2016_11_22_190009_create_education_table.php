<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEducationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('education', function (Blueprint $table) {
            $table->increments('id');
            $table->string('educ_level');
            $table->string('course')->null();
            $table->string('school');
            $table->string('date_from');
            $table->string('date_to');
            $table->integer('member_id')->unsigned();
            $table->timestamps();

            $table->foreign('member_id')->references('id')->on('members')
                            ->onDelete('cascade');

            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('education');
    }
}
