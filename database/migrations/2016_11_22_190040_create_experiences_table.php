<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('experiences', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_name');
            $table->string('role');
            $table->string('job_desc');
            $table->string('date_from');
            $table->string('date_to');
            $table->integer('member_id')->unsigned();
            $table->timestamps();

            $table->foreign('member_id')->references('id')->on('members')
                            ->onDelete('cascade');

            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('experiences');
    }
}
