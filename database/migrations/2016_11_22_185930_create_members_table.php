<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lname');
            $table->string('fname');
            $table->string('mname');
            $table->string('address');
            $table->string('phone');
            $table->date('birthdate');
            $table->string('gender');
            $table->binary('image');
            $table->integer('user_id')->unsigned();
            $table->timestamps();

            $table->SoftDeletes();
        });

        Schema::table('members', function($table){
            $table->foreign('user_id')->references('id')->on('users')
                          ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
