<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Education extends Model
{
  use SoftDeletes;

  protected $dates = ['deleted_at'];

  protected $fillable = [
      'educ_level',
      'course',
      'school',
      'date_from',
      'date_to',
      'member_id'
  ];

  public function member(){
    return $this->belongsTo('App\Member', 'member_id', 'id');
  }
}
