<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
class Member extends Model
{
  use SoftDeletes;

  protected $dates = ['deleted_at'];

  protected $fillable = [
        'lname',
        'fname',
        'mname',
        'address',
        'phone',
        'birthdate',
        'gender',
        'image',
        'user_id'
  ];

  public function user(){
    return $this->belongsTo('App\User', 'role_id', 'id');
  }

  public function skills(){
    return $this->hasMany('App\Skill', 'member_id', 'id');
  }

  public function educations(){
    return $this->hasMany('App\Education', 'member_id', 'id');
  }

  public function achievements(){
    return $this->hasMany('App\Achievement', 'member_id', 'id');
  }

  public function experiences(){
    return $this->hasMany('App\Experience', 'member_id', 'id');
  }
}
