<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Experience extends Model
{
  use SoftDeletes;

  protected $dates = ['deleted_at'];

  protected $fillable = [
      'company_name',
      'role',
      'job_desc',
      'date_from',
      'date_to',
      'member_id'
  ];

  public function member(){
    return $this->belongsTo('App\Member', 'member_id', 'id');
  }
}
