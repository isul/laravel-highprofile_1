<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;


class User extends Authenticatable
{
    use SoftDeletes;
    use Notifiable;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'username', 'password', 'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function member(){
      return $this->hasOne('App\Member', 'user_id', 'id');
    }

    public function members(){
      return $this->hasMany('App\Member', 'user_id', 'id');
    }

    public function admin(){
      return $this->hasOne('App\Admin', 'user_id', 'id');
    }

    public function admins(){
      return $this->hasMany('App\Member', 'user_id', 'id');
    }

    public function role(){
      return $this->belongsTo('App\Role', 'role_id', 'id');
    }

    public function isAdmin(){
      if ($this->role->access_level == "administrator") {
          return true;
      }
      return false;
    }

    public function isMember(){
      if ($this->role->access_level == "member") {
          return true;
      }
      return false;
    }
}
