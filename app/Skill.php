<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Skill extends Model
{
  use SoftDeletes;

  protected $dates = ['deleted_at'];

  protected $fillable = [
        'skill_name',
        'description',
        'level',
        'member_id'
  ];

  public function member(){
    return $this->belongsTo('App\Member', 'member_id', 'id');
  }
}
