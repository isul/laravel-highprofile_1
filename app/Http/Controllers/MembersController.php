<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Member;
use App\Skill;
use App\Education;
use App\Achievement;
use App\Experience;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Image;
use Session;
class MembersController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth','member']);
    }

    public function profile()
    {
        $user_id = Auth::user()->id;
        $member = User::findOrFail($user_id)->member;

        if ($member) {
          $member_id = $member->id;
          $experiences = Member::findOrFail($member_id)->experiences;
          $educations = Member::findOrFail($member_id)->educations;
          $skills = Member::findOrFail($member_id)->skills;
          $achievements =Member::findOrFail($member_id)->achievements;
          return view('profile', compact('member', 'experiences', 'educations', 'skills', 'achievements'));
        }
        return view('profile', compact('member'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('members.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      try {
        $user_id = Auth::user()->id;

        $input = $request->all() + ['user_id' => $user_id];
        $this->validate($request,[
          'lname' => 'required',
          'fname' => 'required',
          'address' => 'required',
          'mname' => 'required',
          'phone' => 'required',
          'gender' => 'required',
          'birthdate' => 'required',
          'image' => 'mimes:jpeg,bmp,png'
        ]);

        if($request['image'] !=  null){

           $img = Image::make($input['image']);
           $img->resize(250, 250);
           $img->response('jpeg');
           $input['image'] = $img;

        }

        Member::create($input);
      } catch (\Exception $e) {
        Session::flash('flash_message','Failed!' . $e);
        return redirect('/newmember');
      }

      Session::flash('flash_message','Successfully stored Information!');
      return redirect('/profile');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $member = Member::findOrFail($id);
      return view('members.edit',compact('member'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $member = Member::findOrFail($id);

      $user_id = Auth::user()->id;

      $input = $request->all() + ['user_id' => $user_id];
      $this->validate($request,[
        'lname' => 'required',
        'fname' => 'required',
        'address' => 'required',
        'mname' => 'required',
        'phone' => 'required',
        'gender' => 'required',
        'birthdate' => 'required',
        'image' => 'mimes:jpeg,bmp,png'
      ]);



      $member->fill($input)->save();
      Session::flash('flash_message','Personal Information Updated');
      return redirect('/profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function softdelete($id)
    {

    }
}
