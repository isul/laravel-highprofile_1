<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Member;
use App\Achievement;
use Session;

class AchievementsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('achievements.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      try {
        $user_id = Auth::user()->id;

        $member = User::findOrFail($user_id)->member;
        $member_id = $member['id'];
        $input = $request->all() + ['member_id' => $member_id];
        $this->validate($request,[
          'event' => 'required',
          'description' => 'required',
          'given_by' => 'required',
          'date_acquired' => 'required'
        ]);

        Achievement::create($input);
      } catch (\Exception $e) {
        Session::flash('flash_message','Failed!' . $e);
        return redirect('/members/experiences/new');
      }

      Session::flash('flash_message','Successfully stored Information!');
      return redirect('/profile');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $achievement = Achievement::findOrFail($id);
      return view('achievements.edit',compact('$achievement'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      try {

        $achievement = Achievement::findOrFail($id);
        $input = $request->all();
        $this->validate($request,[
          'event' => 'required',
          'description' => 'required',
          'given_by' => 'required',
          'date_acquired' => 'required'
        ]);

        $achievement->fill($input)->save();
      } catch (\Exception $e) {
        Session::flash('flash_message','Failed!' . $e);
        return redirect('/members/achievements/edit' . '/'.$id);
      }

      Session::flash('flash_message','Successfully stored Information!');
      return redirect('/profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      try {
        $achievement = Achievement::findOrFail($id);
        $achievement->delete();
        Session::flash('flash_message','Achievement Information Successfully Deleted');
        return redirect('/profile');
      } catch (Exception $e) {
          Session::flash('flash_message','Aw snap!' . $e);
          return redirect('/profile');
      }

    }
}
