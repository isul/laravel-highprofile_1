<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Admin extends Model
{
  use SoftDeletes;

  protected $dates = ['deleted_at'];

  protected $fillable = [
        'lname',
        'fname',
        'mname',
        'address',
        'phone',
        'image',
        'user_id'
  ];

  public function user(){
    return $this->belongsTo('App\User');
  }
}
