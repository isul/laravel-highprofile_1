<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
  use SoftDeletes;

  protected $dates = ['deleted_at'];

  protected $fillable = [
    'access_level'
  ];

  public function user(){
    return $this->hasOne('App\User', 'role_id', 'id');
  }

  public function users(){
    return $this->hasMany('App\User', 'role_id', 'id');
  }
}
