<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Achievement extends Model
{
  use SoftDeletes;

  protected $dates = ['deleted_at'];

  protected $fillable = [
      'event',
      'description',
      'date_acquired',
      'given_by',
      'member_id'
  ];

  public function member(){
    return $this->belongsTo('App\Member', 'member_id', 'id');
  }
}
